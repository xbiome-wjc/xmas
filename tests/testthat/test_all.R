context("Testing all functions in XMAS")
library(XMAS)

context("Checking function da_16s, which could be used for differential analysis of 16S data.")
test_that("da_16s",{
  tmp <- da_16s(simu_16s_data, "Response", method='aldex2')
  expect_equal(tmp$aldex$All_features %>% class(), "data.frame")
  expect_equal(tmp$aldex$DA_features %>% class(), "data.frame")
})

context("Checking function wilcox_mgs, one method of da_mgs.")
test_that("wilcox_mgs",{
  tmp <- wilcox_mgs(mgs_da_data, "Response", pair = FALSE)
  expect_equal(tmp$All_features %>% class(), "data.frame")
  expect_equal(tmp$DA_features %>% class(), "data.frame")
})

context("Checking function da_wilcoxCLR, one method of da_16s.")
test_that("da_wilcoxCLR", {
  tmp <- da_wilcoxCLR(simu_16s_data, "Response")
  expect_equal(tmp$All_features %>% class(), "data.frame")
  expect_equal(tmp$DA_features %>% class(), "data.frame")
})

context("Checking function da_omnibus, one method of da_16s.")
test_that("da_omnibus", {
  tmp <- da_omnibus(simu_16s_data, "Response")
  expect_equal(tmp$All_features %>% class(), "data.frame")
  expect_equal(tmp$DA_features %>% class(), "data.frame")
})

context("Checking function da_mbzinb, one method of da_16s.")
test_that("da_mbzinb", {
  tmp <- da_mbzinb(simu_16s_data,"Response")
  expect_equal(tmp$All_features %>% class(), "data.frame")
  expect_equal(tmp$DA_features %>% class(), "data.frame")
})

context("Checking function da_raida, one method of da_16s.")
test_that("da_raida", {
  tmp <- da_raida(simu_16s_data, "Response")
  expect_equal(tmp$All_features %>% class(), "data.frame")
  expect_equal(tmp$DA_features %>% class(), "data.frame")
})

context("Checking function da_limma, one method of da_16s.")
test_that("da_limma", {
  tmp <- da_limma(simu_16s_data, "Response")
  expect_equal(tmp$All_features %>% class(), "data.frame")
  expect_equal(tmp$DA_features %>% class(), "data.frame")
})

context("Checking function normalization, which could be used to normalize sparse data")
test_that("normalization",{
  df <- as.data.frame(matrix(stats::runif(400),nrow=10))
  tmp <- normalization(df, method = "TSS",taxa_are_rows = TRUE)
  expect_equal(tmp %>% class(), "data.frame")
})

context("Checking function da_aldex2, one method of da_16s.")
test_that("da_aldex2", {
  tmp <- da_aldex2(simu_16s_data, "Response")
  expect_equal(tmp$All_features %>% class(), "data.frame")
  expect_equal(tmp$DA_features %>% class(), "data.frame")
})

context("Checking function metamicrobiomeR_mgs, one method of da_mgs.")
test_that("metamicrobiomeR_mgs", {
  tmp <- metamicrobiomeR_mgs(mgs_da_data,"Response")
  expect_equal(tmp$All_features %>% class(), "data.frame")
  expect_equal(tmp$DA_features %>% class(), "data.frame")
})

context("Checking function lefse_mgs, one method of da_mgs.")
test_that("lefse_mgs", {
  tmp <- lefse_mgs(mgs_da_data,"Response")
  expect_equal(tmp$All_features %>% class(), "data.frame")
  expect_equal(tmp$DA_features %>% class(), "data.frame")
})

context("Checking function ggrare, could be use to draw rarefaction curve.")
test_that("ggrare",{
  good_taxon_table <- data.frame(sum.taxonomy = c("a;b;c;d;f;u", "p;q;r;s;t;u"),
                                 site_1 = c(0,1), site_2 = c(10, 20))
  good_maps <- data.frame(site = c("site_1", "site_2"),
                          season = c("wet", "dry"), host = c("oak", "sage"))
  physeq_object <- ranacapa::convert_anacapa_to_phyloseq(good_taxon_table, good_maps)
  tmp <- ggrare(physeq_object, step = 20, se = TRUE) %>% class()
  expect_equal(tmp %>% class(), "character")
})


context("Checking function filter_prevalence, could be used to filter data with given threshold.")
test_that("filter_prevalence", {
  tmp <- filter_prevalence(gvhd_otu, gvhd_meta, 0.05, taxa_are_rows = TRUE)
  expect_equal(tmp %>% class(), "data.frame")
})

context("Checking function da_deseq2, one method of da_16s.")
test_that("da_deseq2",{
  tmp <- da_deseq2(simu_16s_data,"Response")
  expect_equal(tmp$All_features %>% class(), "data.frame")
  expect_equal(tmp$DA_features %>% class(), "data.frame")
})

context("Checking function convert_metacyc_level, could be used to change the level of Metacyc pathway.")
test_that("convert_metacyc_level",{
  df <- data.frame('Sample_test' = 0.011)
  rownames(df) <- 'PWY0-42'
  tmp <- convert_metacyc_level(df, 3)
  expect_equal(tmp%>% class(), "data.frame")
})

context("Checking function da_mgs, which could be used for differential analysis of MGS data.")
test_that("convert_metacyc_level",{
  tmp <- da_mgs(mgs_rA_data, "Response", method=c("wilcox"),plot=FALSE)
  expect_equal(tmp$wilcox$All_features %>% class(), "data.frame")
})

context("function after2pre, which could be used for FMT project.")
test_that("after2pre", {
  df <- data.frame(row.names =  c("tax1", "tax2","tax3","tax4","tax5"),
                   s1 = c(2,1,3,0,4), s2 = c(1,3,2,0,4),
                   s3 = c(2,2,3,1,4),s4 = c(1,2,3,1,5),s5 = c(2,3,2,1,3))
  g1 <- c("s1","s2")
  g2 <- c("s3","s4","s5")
  tmp <- after2pre(df,g1,g2,0.1,1)
  expect_equal(tmp$data %>% class(), "data.frame")
})

context("function calculate_beta_diversity_mgs, which could be used to calculate beta diversity of MGS data.")
test_that("calculate_beta_diversity_mgs", {
  result <- calculate_beta_diversity_mgs(gvhd_otu, gvhd_meta, threshold = 0.01, taxonomy_level = 'Species')
  expect_equal(result %>% class(), "list")
})

context("function calculate_alpha_diversity_mgs, which could be used to calculate alpha diversity of MGS data.")
test_that("calculate_alpha_diversity_mgs", {
  result <- calculate_alpha_diversity_mgs(gvhd_otu, gvhd_meta, threshold=0.01, measures = "Shannon", taxonomy_level = 'Species',filter_low_abundance=TRUE)
  expect_equal(result %>% class(), "data.frame")
})

context("function filterMGS_abundance, which could be used to filter low abundance MGS data and generate absolute abundance of sample taxas.")
test_that("filterMGS_abundance", {
  result <- filterMGS_abundance(gvhd_otu, gvhd_meta, threshold =0.01 ,input_level = "all",output_level  = "all",filter_bacteria = TRUE)
  expect_equal(result %>% class(), "data.frame")
})

context("function construct_barplot_data, which could be used to construct input data for compositional barplot.")
test_that("construct_barplot_data", {
  result <- construct_barplot_data(gvhd_otu, gvhd_meta, threshold=0.01 ,input_level = "all",output_level  = "Species",filter_bacteria = TRUE, taxa_name_short = TRUE, filter_low_abundance = TRUE)
  expect_equal(result %>% class(), "data.frame")
})

context('Select specified taxa level from metaphlan2.')
test_that('metaphlan2_taxa_Level',{
  result <- metaphlan2_taxa_Level(gvhd_otu, "Species", taxa_name_to_short=TRUE)
  expect_equal(result %>% class(), "data.frame")
})

context('Permanova test of MGS data')
test_that('run_permanova_betadisp',{
  result <- run_permanova_betadisp(mgs_rA_data, permanova =T, betadisp = T, 'Batch')
  expect_equal(result %>% class(), "list")
})
