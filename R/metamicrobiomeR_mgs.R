#' Da metamicrobiomeR DA analysis
#'
#' metamicrobiomeR DA analysis of metagenomics data
#' @param ps Phyloseq object of metagenomics data.
#' @param compvar Groups that will be used for differential analysis, please provide colname in phyloseq's sample_data dataframe. Please check example for details.
#' @return Two dataframes. Original output from metamicrobiomeR and filtered (adj.pvalue < 0.05) output from metamicrobiomeR
#' @import dplyr
#' @import magrittr
#' @import xviz
#' @import MASS
#' @import compositions
#' @import phyloseq
#' @import magrittr
#' @import tidyr
#' @import tibble
#' @import textshape
#' @import metamicrobiomeR
#' @import UpSetR
#' @import ComplexHeatmap
#' @import VennDiagram
#' @import utils
#' @import stringr
#' @import stats
#' @import grid
#' @export
#'
#' @examples
#' \dontrun{compvar <- "Response"
#' example_data <- XMAS:::mgs_da_data
#' metamicrobiomeR_mgs(example_data,compvar)
#' }
metamicrobiomeR_mgs <- function(ps,compvar){

  ### OTU ###
  data <- otu_table(ps)

  if (otu_table(ps)@taxa_are_rows){
    data %<>% t()
  }
  data %<>% as.data.frame()
  data %<>% rownames_to_column("SampleID")

  ### Metadata ###
  metadata = sample_data(ps) %>% as.matrix() %>% as.data.frame()
  SampleID <- NULL
  if(!is.na(match("SampleID",colnames(metadata)))){
    metadata %<>% dplyr::select(-SampleID)
  }
  metadata %<>% rownames_to_column("SampleID")

  ### Run ###
  metamicrobiomeR_data <- merge(metadata %>% dplyr::select(compvar,SampleID),data,by = "SampleID")

  colnames(metamicrobiomeR_data)[1]="personid"
  colnames(metamicrobiomeR_data) <- gsub('\\|','.',colnames(metamicrobiomeR_data))

  if_colnames_start_with_k__ = length(which(str_detect(colnames(metamicrobiomeR_data),"k__")==TRUE))

  if(if_colnames_start_with_k__==0){
    colnames(metamicrobiomeR_data)[which(!(colnames(metamicrobiomeR_data)%in% c("personid",compvar)))] = paste("k__",colnames(metamicrobiomeR_data)[which(!(colnames(metamicrobiomeR_data)%in% c("personid",compvar)))],sep="")
  }

  metamicrobiomeR_result <- taxa.compare(taxtab=metamicrobiomeR_data ,
                                         propmed.rel="gamlss",
                                         adjustvar = NULL,
                                         comvar= compvar,
                                         personid = "personid",
                                         longitudinal="no",
                                         percent.filter = 0,relabund.filter = 0, # no filtering
                                         p.adjust.method="fdr")


  metamicrobiomeR_result$id = gsub('\\.','|',metamicrobiomeR_result$id)

  if(if_colnames_start_with_k__==0){
    metamicrobiomeR_result$id %<>% str_remove("k__")
  }


  metamicrobiomeR_da_result = metamicrobiomeR_result[which(metamicrobiomeR_result[,which(str_detect(colnames(metamicrobiomeR_result),"pval.adjust")==TRUE)]<0.05),]

  metamicrobiomeR_output <- list("All_features" = metamicrobiomeR_result,
                                 "DA_features" = metamicrobiomeR_da_result)

  return(metamicrobiomeR_output)
}
