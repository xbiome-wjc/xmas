#' DA aldex2 for 16S
#' @param ps Phyloseq object of 16S data.
#' @param compvar Groups that will be used for differential analysis, please provide colname in phyloseq's sample_data dataframe. Please check example for details.
#' @import ALDEx2
#' @import phyloseq
#' @import magrittr
#' @import tidyr
#' @import dplyr
#' @import tibble
#' @import textshape
#' @export
#' @examples
#' \dontrun{compvar <- "Response"
#' example_data <- XMAS:::simu_16s_data
#' da_aldex2(example_data,compvar)
#' }
da_aldex2 <- function(ps,compvar){
  SampleID <- wi.eBH <- NULL
  # get otu #
  d_otu <- otu_table(ps) %>% as.data.frame()

  if(!(otu_table(ps)@taxa_are_rows)){
    d_otu %<>% t()
  }

  # get metadata #
  metadata = sample_data(ps) %>% as.matrix() %>% as.data.frame()
  if(!is.na(match("SampleID",colnames(metadata)))){
    metadata %<>% dplyr::select(-SampleID)
  }
  metadata %<>% rownames_to_column("SampleID")

  aldex_result <- aldex(d_otu,metadata[,which(colnames(metadata)==compvar)] %>% as.character(),test="t") %>%
    rownames_to_column("id")

  aldex_da_result <- aldex_result %>% dplyr::filter(wi.eBH<0.05)

  aldex_output <- list("All_features" = aldex_result,
                       "DA_features" = aldex_da_result)

  return(aldex_output)
}
