#' DA 16S
#' This code aims to unify the input of the DA tools for 16S data: phyloseq
#' @param phyloseq_obj Phyloseq object of metagenomics data.
#' @param compvar Groups that will be used for differential analysis, please provide colname in phyloseq's sample_data dataframe. Please check example for details.
#' @param method DA method that will be used for DA analysis, choose from aldex2/deseq2/limma/mbzinb/omnibus/raida/wilcox.
#' @param VennPlot Whether to output VennPlot or not
#' @param VolcanoPlot Whether to output VolcanoPlot or not
#' @param rawOTU Whether input phyloseq contains raw otu table or not
#' @param level Taxonomy level
#' @import dplyr
#' @import magrittr
#' @import xviz
#' @import MASS
#' @import compositions
#' @import phyloseq
#' @import magrittr
#' @import tidyr
#' @import tibble
#' @import textshape
#' @import metamicrobiomeR
#' @import UpSetR
#' @import ComplexHeatmap
#' @import VennDiagram
#' @import utils
#' @import stringr
#' @import stats
#' @import grid
#' @import EnhancedVolcano
#' @export
#' @examples
#' \dontrun{compvar <- "Response"
#' example_data <- XMAS:::da_16s_data
#' da_16s(example_data ,compvar, method='aldex2')
#' }
da_16s <- function(phyloseq_obj, compvar, method,VennPlot=FALSE,VolcanoPlot=NULL,rawOTU=FALSE,level=FALSE){
  log2FC <- log2FoldChange <- pvalue <- logFC <- PValue <- NULL
  da_16s_result = list()
  venn_plot_list = list()
  volcano_list = list()

  selected_level = level

  if(rawOTU==TRUE & level!="OTU"){
   # if(level!="OTU"){
      phyloseq_obj_update <- taxa_level_phyloseq(phyloseq_obj,level=selected_level)
      phyloseq_obj = phyloseq_obj_update
  #  }
  }


  ######### 1.aldex2 ########

  if(!is.na(match("aldex2",method))){
    r_aldex2 = da_aldex2(phyloseq_obj,compvar)
    da_16s_result[["aldex"]] = r_aldex2
    venn_plot_list[["aldex"]]= r_aldex2$DA_features$id
  }


  ######### 2.ancom #########

  # if(!is.na(match("ancom",method))){
  #   r_ancom = da_ancom(phyloseq_obj,compvar)
  #   da_16s_result[["ancom"]] = r_ancom
  #   venn_plot_list[["ancom"]]= r_ancom$DA_features
  # }


  ######### 3.dacomp ########

  # if(!is.na(match("dacomp",method))){
  #   r_dacomp = da_dacomp(phyloseq_obj,compvar)
  #   da_16s_result[["dacomp"]] = r_dacomp
  #   venn_plot_list[["dacomp"]]= r_dacomp$DA_features
  # }



  ######### 4.deseq2 ########

  if(!is.na(match("deseq2",method))){

    r_deseq2 = da_deseq2(phyloseq_obj,compvar)
    da_16s_result[["deseq2"]] = r_deseq2
    venn_plot_list[["deseq2"]]= r_deseq2$DA_features$id

    volcano_list[["deseq2"]] = r_deseq2$All_features %>% as.data.frame() %>% dplyr::select(id,log2FoldChange,pvalue) %>% mutate(tool = "Deseq2")
    colnames(volcano_list[["deseq2"]]) = c("id","log2FC","pvalue","tool")

  }



  ######### 5.edger ########

  # if(!is.na(match("edgeR",method))){
  #   r_edgeR = da_edgeR(phyloseq_obj,compvar)
  #   da_16s_result[["edgeR"]] = r_edgeR
  #   venn_plot_list[["edgeR"]]= r_edgeR$DA_features$id
  #
  #   volcano_list[["edgeR"]] = r_edgeR$All_features %>% dplyr::select(id,logFC,PValue) %>% mutate(method = "edgeR")
  #   colnames(volcano_list[["edgeR"]]) = c("id","log2FC","pvalue","tool")
  # }


  ######### 6.limma ########

  if(!is.na(match("limma",method))){
    P.Value <- NULL
    r_limma = da_limma(phyloseq_obj,compvar)
    da_16s_result[["limma"]] = r_limma
    venn_plot_list[["limma"]]= r_limma$DA_features$id

    volcano_list[["limma"]] = r_limma$All_features %>% dplyr::select(id,logFC,`P.Value`) %>% mutate(method = "limma")
  }


  ####### 7.mbzinb #########

  if(!is.na(match("mbzinb",method))){
    r_mbzinb = da_mbzinb(phyloseq_obj,compvar)
    da_16s_result[["mbzinb"]] = r_mbzinb
    venn_plot_list[["mbzinb"]]= r_mbzinb$DA_features$id

  }

  ########## 8. omnibus ##########

  if(!is.na(match("omnibus",method))){
    r_omnibus = da_omnibus(phyloseq_obj,compvar)
    da_16s_result[["omnibus"]] = r_omnibus
    venn_plot_list[["omnibus"]]= r_omnibus$DA_features$id
  }


  ############ 9. raida ############

  if(!is.na(match("raida",method))){
    r_raida = da_raida(phyloseq_obj,compvar)
    da_16s_result[["raida"]] = r_raida
    venn_plot_list[["raida"]]= r_raida$DA_features$id
  }


  ############ 10.wicoxCLR ############

  if(!is.na(match("wilcox",method))){
    r_wilcox = da_wilcoxCLR(phyloseq_obj,compvar)
    da_16s_result[["wilcox"]] = r_wilcox
    venn_plot_list[["wilcox"]]= r_wilcox$DA_features$id
  }

  ############ 10.wicox ############

#  if(!is.na(match("wilcox",method))){
#    r_wilcox = da_wilcox(phyloseq_obj,compvar)
#    da_16s_result[["wilcox"]] = r_wilcox
#    venn_plot_list[["wilcox"]]= r_wilcox$DA_features$id
#  }

  ############ 11.eBay ############

  # if(!is.na(match("eBay",method))){
  #   r_ebay = da_eBay(phyloseq_obj,compvar)
  #   da_16s_result[["eBay"]] = r_ebay
  #   venn_plot_list[["eBay"]]= r_ebay$DA_features$id
  # }

##### **************************VENN PLOT************************* #####

  if(VennPlot==TRUE){
    color_list = c("deepskyblue","gold1","grey","indianred1","lightgreen")
    # 1. plot venn #
    if(length(method)<=5){
      grid.newpage()
      venn.diagram(venn_plot_list,
                   fill = color_list[1:length(method)],
                   alpha = rep(0.5,length(method)),
                   cat.cex = 1, cex=1, main.cex = 1,main.fontface = "bold",
                   filename=NULL) %>% grid.draw()
    }

    # 2. plot upset #
    if(length(method)>5){
      grid.newpage()
      m = make_comb_mat(venn_plot_list)
      p = UpSet(m,comb_order = order(comb_size(m)))
      print(p)
    }
}

##### **************************VOLCANO PLOT************************* #####

  if(length(VolcanoPlot)>0)
  {
    vol_data = data.frame()
    for(i in 1:length(VolcanoPlot)){
      vol_data = rbind(vol_data,volcano_list[[VolcanoPlot[i]]])
    }
    vol_data %<>% mutate(`-log10P`=-log10(pvalue),
                         point_color = ifelse(abs(log2FC)>1 & pvalue<0.05,"red",
                                              ifelse(abs(log2FC)>1 & pvalue<0.05,"green",
                                                     ifelse(abs(log2FC)<1 & pvalue>0.05,"blue","grey"))))


    p = EnhancedVolcano(vol_data,
                    lab = paste(vol_data$id,vol_data$tool,sep="_"),
                    x = 'log2FC',
                    y = 'pvalue',
                    #xlim = c(-8, 8),
                    #title = 'N061011 versus N61311',
                    pCutoff = 0.05,
                    FCcutoff = 1.5,
                    #col=c('black', 'black', 'black', 'red3'),
                    colAlpha = 1)

    print(p)
  }

  return(da_16s_result)
}
