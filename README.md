# Installation
## Install dependency packages
In R
```
if(!requireNamespace("BiocManager", quietly = TRUE)){
  install.packages("BiocManager")
  }
  
package_list <- c("ALDEx2","ComplexHeatmap","compositions","DelayedMatrixStats","DESeq2","dplyr","edgeR","EnhancedVolcano","ggplot2","GMPR","limma","magrittr","MASS","matrixStats","mbzinb","metagenomeSeq","metamicrobiomeR","nlme","phyloseq","pscl","RAIDA","ranacapa","stringi","stringr","textshape","tibble","tidyr","UpSetR","vegan","VennDiagram","xviz","tidyverse","testthat")

for (p in package_list) {
  if (!suppressWarnings(suppressMessages(require(p, character.only = TRUE, 
                                                 quietly = TRUE, warn.conflicts = FALSE))))
  {
    BiocManager::install(p)
    suppressWarnings(suppressMessages(library(p, character.only = TRUE, 
                                              quietly = TRUE, warn.conflicts = FALSE)))
  }
}

install_github("lichen-lab/GMPR")
install_github("barakbri/dacomp", build_opts = c("--no-resave-data", "--no-manual"))
remotes::install_github("gauravsk/ranacapa")

```
## R dependency packages info
Here is the version information of dependent R packages for XMAS:
```
ALDEx2_1.18.0
ComplexHeatmap_2.2.0
compositions_2.0-2
dacomp_1.26
DelayedMatrixStats_1.8.0
DESeq2_1.26.0
dplyr_1.0.6
edgeR_3.28.1
EnhancedVolcano_1.4.0
ggplot2_3.3.5
GMPR_0.1.3
limma_3.42.2
magrittr_2.0.1
MASS_7.3-54
matrixStats_0.60.0
mbzinb_0.2
metagenomeSeq_1.28.2
metamicrobiomeR_1.1
nlme_3.1-152
phyloseq_1.30.0
pscl_1.5.5
RAIDA_1.0
ranacapa_0.1.0
stringi_1.5.3
stringr_1.4.0
textshape_1.7.3
tibble_3.1.0
tidyr_1.1.3
UpSetR_1.4.0
vegan_2.5-7
VennDiagram_1.6.20
xviz_1.1.0
tidyverse_1.3.1
testthat_3.0.2
```
## Installation instruction
In shell
```
mkdir ~/work_dir
cd ~/work_dir
git clone git@gitlab.com:BangzhuoTong/xmas.git
R CMD build xmas
R CMD INSTALL XMAS_0.0.0.9000.tar.gz
```
# Run
In R
```
library(XMAS)
```
